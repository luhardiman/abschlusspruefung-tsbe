# Vue

## Create Vue App
```bash
npm install -g @vue/cli
npm create vue@latest
```

## Install Vue Router
```bash
npm install vue-router@4
```

```ts
// main.js
import { createApp } from 'vue'
import App from './App.vue'
dimport router from './router/router'

const app = createApp(App).use(router)

app.mount('#app')

```

## Install Pinia
```bash
npm install pinia
```

```ts
// main.js
import { createApp } from 'vue'
import App from './App.vue'
import { createPinia } from 'pinia'

const pinia = createPinia()

const app = createApp(App).use(router).use(pinia)


```

## Add Bootstrap
```bash
npm install bootstrap
npm i bootstrap-icons
```

```ts
// main.js

import "bootstrap/dist/css/bootstrap.min.css"
import 'bootstrap-icons/font/bootstrap-icons.css'
import "bootstrap"
```

## Start App
```bash
npm run dev
```


## State / Store
```javascript
import { defineStore } from 'pinia'

export const useTodoStore = defineStore('app', {
    state: () => ({
        todo: [],
        maxId: 0
    }),
    getters: {
        getTodos(state) {
            return state.todo
        }
    },
    actions: {
        add() {
            this.todo.push('New Todo')
            this._incdrementId()
        },
    },
    _incdrementId() {
        this.maxId++
    }
})
```

## Default Component
```ts
<template>
</template>

<script lang="js">
import { computed, defineComponent } from 'vue'
import { useTodoStore } from '../stores/store';
export default defineComponent({
    name: 'ShowTodo',
    setup() {
        const store = useTodoStore()
        const todos = computed(() => store.getTodos)

        return {
            todos
        }
    }
})
</script>
```

## Child Component
```ts
<template>
    <ShowTodo :todoId="todoId" />
</template>
import ShowTodo from "../components/ShowTodo.vue"
export default defineComponent({
    name: 'Todo',
    components: { ShowTodo },
    props: {  
        todoId: { type: String, required: true},
    },
```

## Router
```ts
import { createWebHistory, createRouter } from 'vue-router'

import Todos from '../views/Todos.vue'

const history = createWebHistory()

const routes = [
  {
    path: '/',
    component: Todos,
    name: 'home',
  },
  {
    path: '/:todoId',
    name: 'todo',
    component: () => import('../views/Todo.vue'),
    props: true
  }
]

const router = createRouter({ history, routes })

export default router
```

## Forms
```ts
<template>
    <input v-model=newTodo>
</template>

import { defineComponent, ref } from 'vue'
export default defineComponent({
  name: 'CreateTodo',
  setup() {

    const newTodo = ref('')


    const addTodo = () => {
        console.log(newTodo.value)
    }

    return {
      newTodo,
      addTodo,
    }
  },
})
```
| Action | Code |
| ------ | ---- |
| Click | `@click="addTodo"` |
| If | `v-if="newTodo"` |
| For | `v-for="todo in todos" :key="todo.id"` |
| For | `v-for="todo in todos" :key="todo.id" :to="/test"` |
