# Overload
```c#
// Vererbung von Base Klasse
class Artikel: Base {
    public int Id { get; set; }
    public string Name { get; set; }
    
    // Overload Method
    public Artikel(int id, string name) {
        Id = id;
        Name = name;
    }

    public Artikel(int id) {
        Id = id;
        Name = "";
    }
}
```

# Interface
```c#
public Interface IArtikel {
    int Id { get; set; }
    string Name { get; set; }
    void Display();
}

public class Artikel: IArtikel {
    public int Id { get; set; }
    public string Name { get; set; }
    public void Display() {
        Console.WriteLine($"Id: {Id}, Name: {Name}");
    }
}
```

# ORM
## Installation
```bash
Install-Package Microsoft.EntityFrameworkCore
```
# Model
```c#
// Model
public class Artikel {
    public int Id { get; set; }
    public string Name { get; set; }
}

public class Store {
    public int Id { get; set; }
    public string Name { get; set; }
    public Artikel[] Artikels { get; set; }
}
// DbContext
public class AppDbContext: DbContext {
    public DbSet<Artikel> Artikels { get; set; }
    public DbSet<Store> Stores { get; set; }

    // DB connection
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
        optionsBuilder.UseSqlite("Host=localhost;Database=prog-two;Username=admin;Password=admin");
        base.OnConfiguring(optionsBuilder);
    }

    // Foreign Keys
    protected override void OnModelCreating(ModelBuilder modelBuilder) {
        // Set Primary Key 
        modelBuilder.Entity<Artikel>().HasKey(a => a.Id);
        modelBuilder.Entity<Store>().HasKey(s => s.Id);
        
        modelBuilder.Entity<Store>().HasMany(s => s.Artikels);
        base.OnModelCreating(modelBuilder);
    }
}
```

More Rules here: https://medium.com/@josiahmahachi/single-responsibility-principle-in-entity-framework-configurations-d86012eeab44

# Migration
```bash
Add-Migration InitialCreate
Update-Database
```

# ORM Usage
```c#
// Create
using (var db = new AppDbContext()) {
    db.Artikels.Add(new Artikel { Name = "Artikel 1" });
    // Save happens here
    db.SaveChanges();

    // Read
    var artikels = db.Artikels.ToList();

    // Update
    var artikel = db.Artikels.Find(1); // Or FirstOrDefault()
    artikel.Name = "Artikel 2";
    context.SaveChanges();

    // Delete
    var artikel = db.Artikels.Find(1);
    db.Artikels.Remove(artikel);
    db.SaveChanges();
}
```

# LINQ
```c#
var artikels = db.Artikels.Where(a => a.Name.Contains("Artikel")).ToList();
```